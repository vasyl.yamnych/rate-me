import sbt._

object Dependencies {
  private val finchXVersion = "0.28.0"
  private val circeVersion = "0.11.1"
  private val scalaTestVersion = "3.0.5"
  private val monixVersion = "3.0.0-RC2"
  private val doobieVersion = "0.7.0-M3"
  private val pureconfigVersion = "0.10.2"
  private val logbackVersion = "1.2.3"
  private val scalaLoggingVersion = "3.9.2"
  private val gatlingVersion = "3.0.3"

  lazy val pureconfig = "com.github.pureconfig" %% "pureconfig" % pureconfigVersion
  lazy val logback = "ch.qos.logback" % "logback-classic" % logbackVersion
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion
  lazy val finchXCore = "com.github.finagle" %% "finchx-core" % finchXVersion
  lazy val finchXCirce = "com.github.finagle" %% "finchx-circe" % finchXVersion
  lazy val circe = "io.circe" %% "circe-generic" % circeVersion
  lazy val monix = "io.monix" %% "monix" % monixVersion
  lazy val doobieCore = "org.tpolecat" %% "doobie-core" % doobieVersion
  lazy val doobiePostgres = "org.tpolecat" %% "doobie-postgres" % doobieVersion
  lazy val doobieTest = "org.tpolecat" %% "doobie-scalatest" % doobieVersion % Test
  lazy val scalaTest = "org.scalatest" %% "scalatest" % scalaTestVersion % "it, test"
  lazy val gatlingCharts = "io.gatling.highcharts" % "gatling-charts-highcharts" % gatlingVersion % IntegrationTest
  lazy val gatlingTest = "io.gatling" % "gatling-test-framework" % gatlingVersion % IntegrationTest
}

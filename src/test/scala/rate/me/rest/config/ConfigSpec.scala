package rate.me.rest.config

import rate.me.config.{ConfigReader, Jdbc, Service}
import pureconfig.generic.auto._
import org.scalatest.FreeSpec
import org.scalatest.Matchers._

class ConfigSpec extends FreeSpec {

  "whole config should be loaded correctly" in {
    pureconfig.loadConfig[ConfigReader] should matchPattern { case Right(ConfigReader(_, _)) => }
  }

  case class JdbcReader(jdbc: Jdbc)
  "jdbc config should be loaded correctly" in {
    pureconfig.loadConfig[JdbcReader] should matchPattern { case Right(JdbcReader(_)) => }
  }

  case class ServiceReader(service: Service)
  "service config should be loaded correctly" in {
    pureconfig.loadConfig[ServiceReader] should matchPattern { case Right(ServiceReader(_)) => }
  }
}

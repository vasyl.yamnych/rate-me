package rate.me.rest.ware.success

import rate.me.rest.utils.PostMethodTests
import rate.me.rest.ware.Helper.{body, rightPaths, _}
import rate.me.rest.ware.PostMethod._

class PostMethodSpec extends PostMethodTests.SuccessTests(body, postEndpoints, rightPaths) {}

package rate.me.rest.ware.success

import rate.me.rest.utils.GetMethodTests
import rate.me.rest.ware.Helper._

class GetMethodSpec extends GetMethodTests.SuccessTests(getEndpoints, rightPathsWithRightParams) {}

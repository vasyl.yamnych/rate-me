package rate.me.rest.ware
import cats.data.NonEmptyList
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import monix.eval.Task
import rate.me.handler.{HandlerP1, HandlerP2EmptyBody}
import rate.me.rest.Utils._
import rate.me.rest.account.General.AccountId
import rate.me.rest.ware.General.{FullImage, ImageUri, WareId, WareName}
import rate.me.rest.ware.GetMethod.{GetResp, Ware}
import rate.me.rest.ware.PostMethod.{PostReq, PostResp}

object Helper {
  val waresPath = "/accounts/10/wares"
  val (rightPaths, wrongPaths) = genRightWrongPaths(waresPath)
  val requiredParams = List(UriParam("ids", NonEmptyList.of("1", "2")))
  val (rightParamsSets, wrongParamsSets) = genRightWrongParams(requiredParams, List())
  val rightPathsWithRightParams = for {
    params <- rightParamsSets
  } yield addParamsToPath(waresPath, params)
  val rightPathsWithWrongParams = for {
    params <- wrongParamsSets
  } yield addParamsToPath(waresPath, params)

  val body = PostReq(WareName("medalyons"), FullImage("image1"))
  case class BadReq(name: String, imageBad: String)
  val bodyBad = BadReq("medalyons", "image1")

  implicit val decodeBadReq: Decoder[BadReq] = deriveDecoder[BadReq]
  implicit val encodeBadReq: Encoder[BadReq] = deriveEncoder[BadReq]

  val postHandler = new HandlerP1[Task, AccountId, PostReq, PostResp] {
    override def apply(accountId: AccountId, req: PostReq): Task[PostResp] = Task { PostResp(WareId(10)) }
  }
  val postEndpoints = PostMethod(postHandler).endpoints

  val wares = Array(Ware(AccountId(10), WareId(80), WareName("acc10"), ImageUri("http://image1")))
  val getHandler = new HandlerP2EmptyBody[Task, AccountId, NonEmptyList[WareId], GetResp] {
    override def apply(accountId: AccountId, wareIds: NonEmptyList[WareId]): Task[GetResp] = Task {
      GetResp(wares)
    }
  }
  val getEndpoints = GetMethod(getHandler).endpoints

  val allEndpoints = AllMethods(PostMethod(postHandler), GetMethod(getHandler)).endpoints
}

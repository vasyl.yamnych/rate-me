package rate.me.rest.ware.failure
import rate.me.rest.utils.PostMethodTests
import rate.me.rest.ware.Helper._
import rate.me.rest.ware.PostMethod._

class PostMethodSpec extends PostMethodTests.FailTests(body, bodyBad, postEndpoints, rightPaths, wrongPaths) {}

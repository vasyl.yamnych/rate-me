package rate.me.rest.ware.failure

import rate.me.rest.utils.GetMethodTests
import rate.me.rest.ware.Helper._

class GetMethodSpec extends GetMethodTests.FailTests(getEndpoints, wrongPaths, rightPaths, rightPathsWithWrongParams) {}

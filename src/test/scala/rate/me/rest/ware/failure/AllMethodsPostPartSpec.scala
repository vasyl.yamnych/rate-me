package rate.me.rest.ware.failure
import rate.me.rest.utils.PostMethodTests
import rate.me.rest.ware.Helper.{allEndpoints, body, bodyBad, rightPaths, wrongPaths}

class AllMethodsPostPartSpec extends PostMethodTests.FailTests(body, bodyBad, allEndpoints, rightPaths, wrongPaths) {}

package rate.me.rest.ware.failure
import rate.me.rest.utils.GetMethodTests
import rate.me.rest.ware.Helper._

class AllMethodsGetPartSpec
    extends GetMethodTests.FailTests(allEndpoints, wrongPaths, rightPaths, rightPathsWithWrongParams) {}

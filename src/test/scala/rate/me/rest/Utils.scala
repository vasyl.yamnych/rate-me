package rate.me.rest
import cats.data.NonEmptyList
import io.finch.Endpoint.Result
import io.finch.{Application, Encode, Endpoint, Input}
import monix.eval.Task
import shapeless.{Inl, Inr}

import scala.reflect.ClassTag
import scala.util.Random

object Utils {

  implicit class RichEndpoint[R](endpoint: Endpoint[Task, R]) {
    def postJson[A: Encode.Json](path: String, body: A): Result[Task, R] =
      endpoint(Input.post(path).withBody[Application.Json](body))

    def getJson(path: String): Result[Task, R] =
      endpoint(Input.get(path))

    def check[RESP: ClassTag]: PartialFunction[Any, _] = {
      val ct = implicitly[ClassTag[RESP]]
      val pf: PartialFunction[Any, _] = {
        case Some(Right(resp)) if ct.runtimeClass.isInstance(resp)                                         =>
        case Some(Right(Inl(resp))) if ct.runtimeClass.isInstance(resp)                                    =>
        case Some(Right(Inr(Inl(resp)))) if ct.runtimeClass.isInstance(resp)                               =>
        case Some(Right(Inr(Inr(Inl(resp))))) if ct.runtimeClass.isInstance(resp)                          =>
        case Some(Right(Inr(Inr(Inr(Inl(resp)))))) if ct.runtimeClass.isInstance(resp)                     =>
        case Some(Right(Inr(Inr(Inr(Inr(Inl(resp))))))) if ct.runtimeClass.isInstance(resp)                =>
        case Some(Right(Inr(Inr(Inr(Inr(Inr(Inl(resp)))))))) if ct.runtimeClass.isInstance(resp)           =>
        case Some(Right(Inr(Inr(Inr(Inr(Inr(Inr(Inl(resp))))))))) if ct.runtimeClass.isInstance(resp)      =>
        case Some(Right(Inr(Inr(Inr(Inr(Inr(Inr(Inr(Inl(resp)))))))))) if ct.runtimeClass.isInstance(resp) =>
      }
      pf
    }
  }

  def genRightWrongPaths(path: String): (List[String], List[String]) =
    (List(path, s"$path/"), List(s"$path//", s"$path/abc"))

  case class UriParam(name: String, values: NonEmptyList[String])

  def genRightWrongParams(requiredParams: List[UriParam],
                          optionalParams: List[UriParam]): (List[List[UriParam]], List[List[UriParam]]) = {
    //TODO add not resolved, wrong param
    val rightParamsCombs = (List(requiredParams, optionalParams ++ requiredParams) ++
      optionalParams.map(_ :: requiredParams)).distinct
    val wrongParamsCombs = requiredParams match {
      case Nil      => List()
      case _ :: Nil => List()
      case _ :: _   => requiredParams.map(List(_))
    }
    (rightParamsCombs, wrongParamsCombs)
  }

  def convertToUriParams(params: List[UriParam]): String = {
    val elems: List[String] = for {
      p <- params
      values = p.values.toList
      v <- values
    } yield s"${p.name}=$v"
    Random.shuffle(elems).foldLeft("")((sum, el) => s"$sum&$el")
  }

  def addParamsToPath(path: String, params: List[UriParam]): String = {
    convertToUriParams(params) match {
      case "" => path
      case uriParams =>
        path.lastOption match {
          case Some('/') => s"$path?$uriParams"
          case Some(_)   => s"$path/?$uriParams"
          case None      => path
        }
    }
  }
}

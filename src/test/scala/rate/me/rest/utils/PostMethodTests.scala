package rate.me.rest.utils

import io.circe.Encoder
import io.circe.syntax._
import io.finch.Endpoint
import io.finch.Error.NotParsed
import io.finch.circe._
import monix.eval.Task
import monix.execution.Scheduler.Implicits.global
import org.scalatest.FreeSpec
import org.scalatest.Matchers._
import rate.me.rest.Utils._

import scala.reflect.ClassTag

object PostMethodTests {
  abstract class SuccessTests[REQ: Encoder, RESP: ClassTag](body: REQ,
                                                            endpoints: Endpoint[Task, RESP],
                                                            rightPaths: List[String])
      extends FreeSpec {
    rightPaths.foreach { path =>
      s"POST $path with correct body ${body.asJson.noSpaces} succeed" in {
        endpoints.postJson(path, body).awaitValue() should
          matchPattern { endpoints.check[RESP] }
      }
    }
  }

  abstract class FailTests[REQ: Encoder, REQBAD: Encoder, RESP: ClassTag](body: REQ,
                                                                          bodyBad: REQBAD,
                                                                          endpoints: Endpoint[Task, RESP],
                                                                          rightPaths: List[String],
                                                                          wrongPaths: List[String])
      extends FreeSpec {
    wrongPaths.foreach { path =>
      s"POST $path with correct body ${body.asJson.noSpaces} fails" in {
        endpoints.postJson(path, body).awaitValue() should
          matchPattern { case None => }
      }
    }

    rightPaths.foreach { path =>
      s"POST $path with wrong body ${bodyBad.asJson.noSpaces} fails" in {
        endpoints.postJson(path, bodyBad).awaitValue() should
          matchPattern { case Some(Left(NotParsed(_, _, _))) => }
      }
    }
  }
}

package rate.me.rest.utils

import io.finch.Endpoint
import io.finch.Error.NotPresent
import monix.eval.Task
import monix.execution.Scheduler.Implicits.global
import org.scalatest.FreeSpec
import org.scalatest.Matchers._
import rate.me.rest.Utils._

import scala.reflect.ClassTag

object GetMethodTests {

  abstract class SuccessTests[R: ClassTag](endpoints: Endpoint[Task, R], rightPathsWithRightParams: List[String])
      extends FreeSpec {
    rightPathsWithRightParams.foreach { path =>
      s"GET $path succeed" in {
        endpoints.getJson(path).awaitValue() should
          matchPattern { endpoints.check[R] }
      }
    }
  }

  abstract class FailTests[R](endpoints: Endpoint[Task, R],
                              wrongPaths: List[String],
                              rightPathsWithoutParams: List[String],
                              rightPathsWithWrongParams: List[String])
      extends FreeSpec {

    wrongPaths.foreach { path =>
      s"GET $path fails" in {
        endpoints.getJson(path).awaitValue() should
          matchPattern { case None => }
      }
    }

    (rightPathsWithoutParams ++ rightPathsWithWrongParams).foreach { path =>
      s"GET $path fails" in {
        endpoints.getJson(path).awaitValue() should
          matchPattern { case Some(Left(NotPresent(_))) => }
      }
    }
  }
}

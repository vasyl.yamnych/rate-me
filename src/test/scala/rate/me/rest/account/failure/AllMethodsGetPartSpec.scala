package rate.me.rest.account.failure
import rate.me.rest.account.Helper._
import rate.me.rest.utils.GetMethodTests

class AllMethodsGetPartSpec
    extends GetMethodTests.FailTests(allEndpoints, wrongPaths, rightPaths, rightPathsWithWrongParams) {}

package rate.me.rest.account.failure
import rate.me.rest.account.Helper.{body, bodyBad, postEndpoints, rightPaths, wrongPaths}
import rate.me.rest.utils.PostMethodTests

class PostMethodSpec extends PostMethodTests.FailTests(body, bodyBad, postEndpoints, rightPaths, wrongPaths) {}

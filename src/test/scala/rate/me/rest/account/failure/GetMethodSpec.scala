package rate.me.rest.account.failure
import rate.me.rest.account.Helper.{rightPaths, rightPathsWithWrongParams, wrongPaths, _}
import rate.me.rest.utils._

class GetMethodSpec extends GetMethodTests.FailTests(getEndpoints, wrongPaths, rightPaths, rightPathsWithWrongParams) {}

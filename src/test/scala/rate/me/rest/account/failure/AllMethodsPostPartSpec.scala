package rate.me.rest.account.failure
import rate.me.rest.account.Helper.{allEndpoints, body, bodyBad, rightPaths, wrongPaths}
import rate.me.rest.utils.PostMethodTests

class AllMethodsPostPartSpec extends PostMethodTests.FailTests(body, bodyBad, allEndpoints, rightPaths, wrongPaths) {}

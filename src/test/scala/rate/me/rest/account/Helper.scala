package rate.me.rest.account

import cats.data.NonEmptyList
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import monix.eval.Task
import rate.me.handler.{Handler, HandlerP1EmptyBody}
import rate.me.rest.Utils.{UriParam, addParamsToPath, genRightWrongParams, genRightWrongPaths}
import rate.me.rest.account.General.{AccountId, AccountName}
import rate.me.rest.account.GetMethod.{Account, GetResp}
import rate.me.rest.account.PostMethod.{PostReq, PostResp}

object Helper {
  val accountsPath = "/accounts"
  val (rightPaths, wrongPaths) = genRightWrongPaths(accountsPath)
  val requiredParams = List(UriParam("ids", NonEmptyList.of("1", "2")))
  val (rightParamsSets, wrongParamsSets) = genRightWrongParams(requiredParams, List())
  val rightPathsWithRightParams = for {
    params <- rightParamsSets
  } yield addParamsToPath(accountsPath, params)
  val rightPathsWithWrongParams = for {
    params <- wrongParamsSets
  } yield addParamsToPath(accountsPath, params)

  val body = PostReq(AccountName("Mango"))
  case class BadReq(badName: String)
  val bodyBad = BadReq("Mango")

  implicit val decodeBadReq: Decoder[BadReq] = deriveDecoder[BadReq]
  implicit val encodeBadReq: Encoder[BadReq] = deriveEncoder[BadReq]

  val postHandler = new Handler[Task, PostReq, PostResp] {
    override def apply(req: PostReq): Task[PostResp] = Task { PostResp(AccountId(10)) }
  }
  val postEndpoints = PostMethod(postHandler).endpoints

  val accounts = Array(Account(AccountId(10), AccountName("acc10")))
  val getHandler = new HandlerP1EmptyBody[Task, NonEmptyList[AccountId], GetResp] {
    override def apply(accountIds: NonEmptyList[AccountId]): Task[GetResp] = Task { GetResp(accounts) }
  }
  val getEndpoints = GetMethod(getHandler).endpoints

  val allEndpoints = AllMethods(PostMethod(postHandler), GetMethod(getHandler)).endpoints
}

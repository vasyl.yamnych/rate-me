package rate.me.rest.account.success

import rate.me.rest.account.Helper._
import rate.me.rest.account.PostMethod._
import rate.me.rest.utils.PostMethodTests

class PostMethodSpec extends PostMethodTests.SuccessTests(body, postEndpoints, rightPaths) {}

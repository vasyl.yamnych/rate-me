package rate.me.rest.account.success

import rate.me.rest.utils.GetMethodTests
import rate.me.rest.ware.Helper.{getEndpoints, rightPathsWithRightParams}

class GetMethodSpec extends GetMethodTests.SuccessTests(getEndpoints, rightPathsWithRightParams) {}

package rate.me.rest.account.success
import rate.me.rest.account.Helper._
import rate.me.rest.utils.GetMethodTests
import rate.me.rest.account.Helper.rightPathsWithRightParams

class AllMethodsGetPartSpec extends GetMethodTests.SuccessTests(allEndpoints, rightPathsWithRightParams) {}

CREATE SCHEMA develop;

SET search_path = develop;

CREATE SEQUENCE account_account_id_seq
    START WITH 1 INCREMENT BY 1
    NO MINVALUE NO MAXVALUE CACHE 1;

CREATE SEQUENCE ware_ware_id_seq
    START WITH 1 INCREMENT BY 1
    NO MINVALUE NO MAXVALUE CACHE 1;

CREATE TABLE account(
  account_id integer DEFAULT nextval('account_account_id_seq'::regclass) PRIMARY KEY,
  account_name VARCHAR (50) NOT NULL
);

CREATE TABLE ware(
  ware_id integer DEFAULT nextval('ware_ware_id_seq'::regclass) PRIMARY KEY,
  ware_name VARCHAR (50) NOT NULL,
  ware_image_uri VARCHAR (255) NOT NULL,
  account_id integer NOT NULL,
  CONSTRAINT ware_account_id_fkey FOREIGN KEY (account_id)
      REFERENCES account(account_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE INDEX idx_fk_account_id ON ware USING btree (account_id);

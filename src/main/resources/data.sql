SET search_path = develop;

INSERT INTO account(account_name) VALUES ('account1'), ('account2');

INSERT INTO ware(account_id, ware_name, ware_image_uri) VALUES (1, 'ware1', 'http://image1'), (2, 'ware2', 'http://image2');
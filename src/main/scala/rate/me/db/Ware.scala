package rate.me.db

case class Ware(account_id: Int, ware_id: Int, ware_name: String, ware_image_uri: String)

// We need a scheduler whenever asynchronous
// execution happens, substituting your ExecutionContext
import monix.execution.Scheduler.Implicits.global

// Needed below
import scala.concurrent.Await
import scala.concurrent.duration._

import monix.eval._
// A specification for evaluating a sum,
// nothing gets triggered at this point!
val task = Task { 1 + 1 }
// Actual execution, making use of the Scheduler in
// our scope, imported above
val future = task.runToFuture
// Avoid blocking; this is done for demostrative purposes
Await.result(future, 5.seconds)

import monix.reactive._
// Nothing happens here, as observable is lazily
// evaluated only when the subscription happens!
val tick = {
  Observable.interval(1.millis)
    // common filtering and mapping
    .filter(_ % 2 == 0)
    .map(_ * 2)
    // any respectable Scala type has flatMap, w00t!
    .flatMap(x => Observable.fromIterable(Seq(x,x)))
    // only take the first 5 elements, then stop
    .take(10)
    // to print the generated events to console
    .dump("Out")
}

val cancelable = tick.subscribe()
// Or maybe we change our mind
//cancelable.cancel()

val future2 = task.runToFuture
// Avoid blocking; this is done for demonstrative purposes
Await.result(future2, 5.seconds)

package rate.me.handler.ware

import cats.~>
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import rate.me.handler.HandlerP1
import rate.me.rest.account.General.AccountId
import rate.me.rest.ware.General.WareId
import rate.me.rest.ware.PostMethod.{PostReq, PostResp}

class PostHandler[M[_]](transact: ConnectionIO ~> M) extends HandlerP1[M, AccountId, PostReq, PostResp] {

  override def apply(accountId: AccountId, req: PostReq): M[PostResp] = transact {
    insert(accountId, req).map(id => PostResp(WareId(id)))
  }

  private def insert(accountId: AccountId, req: PostReq) = //TODO BUG put ImageUri instead of FullImage
    sql"INSERT INTO ware(account_id, ware_name, ware_image_uri) VALUES (${accountId.value}, ${req.name.value}, ${req.image.value})".update
      .withUniqueGeneratedKeys[Int]("ware_id")
}

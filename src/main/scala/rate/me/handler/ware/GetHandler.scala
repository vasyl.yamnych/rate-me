package rate.me.handler.ware
import cats.data.NonEmptyList
import cats.~>
import doobie.Fragments._
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import rate.me.db.Ware
import rate.me.handler.HandlerP2EmptyBody
import rate.me.rest.account.General.AccountId
import rate.me.rest.ware.General.{ImageUri, WareId, WareName}
import rate.me.rest.ware.GetMethod
import rate.me.rest.ware.GetMethod.GetResp

class GetHandler[M[_]](transact: ConnectionIO ~> M)
    extends HandlerP2EmptyBody[M, AccountId, NonEmptyList[WareId], GetResp] {

  override def apply(accountId: AccountId, wareIds: NonEmptyList[WareId]): M[GetResp] = transact {
    select(accountId, wareIds).map(arr => GetResp(arr.map(wareDbToWare)))
  }

  private def select(accountId: AccountId, wareIds: NonEmptyList[WareId]) = {
    val q = fr"""
              SELECT account_id, ware_id, ware_name, ware_image_uri
              FROM ware
              WHERE account_id = ${accountId.value} AND """ ++ in(fr"ware_id", wareIds.map(_.value))
    q.query[Ware].to[Array]
  }

  implicit def wareDbToWare(ware: Ware): GetMethod.Ware =
    GetMethod.Ware(AccountId(ware.account_id),
                   WareId(ware.ware_id),
                   WareName(ware.ware_name),
                   ImageUri(ware.ware_image_uri))
}

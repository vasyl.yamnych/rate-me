package rate.me.handler

trait Handler[M[_], REQ, RESP] {
  def apply(req: REQ): M[RESP]
}

trait HandlerP1[M[_], P, REQ, RESP] {
  def apply(param: P, req: REQ): M[RESP]
}

trait HandlerP1EmptyBody[M[_], P, RESP] {
  def apply(param: P): M[RESP]
}

trait HandlerP2[M[_], P1, P2, REQ, RESP] {
  def apply(param1: P1, param2: P2, req: REQ): M[RESP]
}

trait HandlerP2EmptyBody[M[_], P1, P2, RESP] {
  def apply(param1: P1, param2: P2): M[RESP]
}

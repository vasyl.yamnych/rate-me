package rate.me.handler.account

import cats.data.NonEmptyList
import cats.~>
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import rate.me.db.Account
import rate.me.handler.HandlerP1EmptyBody
import rate.me.rest.account.General.{AccountId, AccountName}
import rate.me.rest.account.GetMethod
import rate.me.rest.account.GetMethod.GetResp
import doobie.Fragments._

class GetHandler[M[_]](transact: ConnectionIO ~> M) extends HandlerP1EmptyBody[M, NonEmptyList[AccountId], GetResp] {

  override def apply(accountIds: NonEmptyList[AccountId]): M[GetResp] = transact {
    select(accountIds).map(arr => GetResp(arr.map(accDbToAcc)))
  }

  private def select(accountIds: NonEmptyList[AccountId]) = {
    val q = fr"""SELECT account_id, account_name
              FROM account
              WHERE """ ++ in(fr"account_id", accountIds.map(_.value))
    q.query[Account].to[Array]
  }

  implicit def accDbToAcc(acc: Account): GetMethod.Account =
    GetMethod.Account(AccountId(acc.account_id), AccountName(acc.account_name))
}

package rate.me.handler.account

import cats.~>
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import rate.me.handler.Handler
import rate.me.rest.account.General.AccountId
import rate.me.rest.account.PostMethod.{PostReq, PostResp}

class PostHandler[M[_]](transact: ConnectionIO ~> M) extends Handler[M, PostReq, PostResp] {

  override def apply(req: PostReq): M[PostResp] = transact {
    insert(req).map(id => PostResp(AccountId(id)))
  }

  private def insert(req: PostReq) =
    sql"INSERT INTO account(account_name) VALUES (${req.name.value})".update
      .withUniqueGeneratedKeys[Int]("account_id")
}

package rate.me
import com.twitter.finagle.Http
import com.twitter.util.{Await, Try}
import com.typesafe.scalalogging.StrictLogging
import doobie._
import io.circe.generic.auto._
import io.finch.circe._
import monix.eval.Task
import monix.execution.Scheduler.Implicits.global
import rate.me.config.ConfigReader
import pureconfig.generic.auto._

object Main extends App with StrictLogging {
  pureconfig.loadConfig[ConfigReader] match {
    case Right(config) =>
      val jdbc = config.jdbc
      val xa = Transactor.fromDriverManager[Task](jdbc.driver, jdbc.url, jdbc.user, jdbc.pass)

      val endpoints = rest.account
        .AllMethods(
          rest.account.PostMethod(new handler.account.PostHandler(xa.transB)),
          rest.account.GetMethod(new handler.account.GetHandler(xa.transB))
        )
        .endpoints :+:
        rest.ware
        .AllMethods(
          rest.ware.PostMethod(new handler.ware.PostHandler(xa.transB)),
          rest.ware.GetMethod(new handler.ware.GetHandler(xa.transB))
        )
        .endpoints
      logger.info(s"Starting service at ${config.service.uri}")
      Try(Await.ready(Http.server.serve(config.service.uri, endpoints.toService))).onFailure { error =>
        logger.error("Can't start service", error)
      }
    case Left(config) => logger.error("Wrong config for a service", config)
  }
}

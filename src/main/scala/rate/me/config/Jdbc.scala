package rate.me.config

case class Jdbc(driver: String, url: String, user: String, pass: String)

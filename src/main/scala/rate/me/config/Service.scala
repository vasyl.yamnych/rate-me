package rate.me.config

case class Service(host: String, port: Int) {
  def uri: String = s"$host:$port"
}

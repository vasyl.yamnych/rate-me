package rate.me
import io.finch.EndpointModule
import monix.eval.Task

object MonixEndpointModule extends EndpointModule[Task] {}

import doobie._
import doobie.implicits._
import monix.eval.Task

import scala.concurrent.Await
import scala.concurrent.duration._
import monix.execution.Scheduler.Implicits.global

val xa = Transactor.fromDriverManager[Task](
  "org.postgresql.Driver", // driver classname
  "jdbc:postgresql:postgres", // connect URL (driver-specific)
  "postgres",              // user
  ""                       // password
)

val program2 = sql"select 42".query[Int].unique
val task2 = program2.transact(xa)

Await.result(task2.runToFuture, 5.seconds)

val program3 =
  for {
    a <- sql"select 42".query[Int].unique
    b <- sql"select random()".query[Double].unique
  } yield (a, b)
val task3 = program3.transact(xa)

Await.result(task3.runToFuture, 5.seconds)

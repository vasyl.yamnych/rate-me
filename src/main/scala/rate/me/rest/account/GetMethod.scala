package rate.me.rest.account
import cats.data.NonEmptyList
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import io.finch.Endpoint
import monix.eval.Task
import rate.me.MonixEndpointModule.get
import rate.me.handler.HandlerP1EmptyBody
import rate.me.rest.account.General._
import rate.me.rest.account.GetMethod.GetResp

case class GetMethod(handler: HandlerP1EmptyBody[Task, NonEmptyList[AccountId], GetResp]) {
  val endpoints: Endpoint[Task, GetResp] =
    get(accsPathWithParams).mapAsync { nel =>
      handler(nel)
    }
}

object GetMethod {
  case class Account(id: AccountId, name: AccountName)
  case class GetResp(accounts: Array[Account])

  implicit val decodeAccount: Decoder[Account] = deriveDecoder[Account]
  implicit val encodeAccount: Encoder[Account] = deriveEncoder[Account]

  implicit val decodeGetResp: Decoder[GetResp] = deriveDecoder[GetResp]
  implicit val encodeGetResp: Encoder[GetResp] = deriveEncoder[GetResp]
}

package rate.me.rest.account
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import io.finch.Endpoint
import io.finch.circe._
import monix.eval.Task
import rate.me.MonixEndpointModule.{jsonBody, post}
import rate.me.handler.Handler
import rate.me.rest.account.General._
import rate.me.rest.account.PostMethod._

case class PostMethod(handler: Handler[Task, PostReq, PostResp]) {
  val endpoints: Endpoint[Task, PostResp] =
    post(accsPath :: jsonBody[PostReq]).mapAsync { handler(_) }
}

object PostMethod {
  case class PostReq(name: AccountName)
  case class PostResp(id: AccountId)

  implicit val decodePostReq: Decoder[PostReq] = deriveDecoder[PostReq]
  implicit val encodePostReq: Encoder[PostReq] = deriveEncoder[PostReq]

  implicit val decodePostResp: Decoder[PostResp] = deriveDecoder[PostResp]
  implicit val encodePostResp: Encoder[PostResp] = deriveEncoder[PostResp]
}

package rate.me.rest.account
import io.circe.{Decoder, Encoder}
import rate.me.MonixEndpointModule._

object General {
  val accsPath = path("accounts") :: pathEmpty
  val accsPathWithParams = accsPath :: paramsNel[AccountId]("ids")

  case class AccountName(value: String) extends AnyVal
  case class AccountId(value: Int) extends AnyVal

  implicit val decodeAccountName: Decoder[AccountName] =
    Decoder.decodeString.emap[AccountName](v => Right(AccountName(v)))
  implicit val encodeAccountName: Encoder[AccountName] =
    Encoder.encodeString.contramap[AccountName](_.value)

  implicit val decodeAccountId: Decoder[AccountId] =
    Decoder.decodeInt.emap[AccountId](v => Right(AccountId(v)))
  implicit val encodeAccountId: Encoder[AccountId] =
    Encoder.encodeInt.contramap[AccountId](_.value)
}

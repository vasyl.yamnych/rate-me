package rate.me.rest.account

case class AllMethods(postMethod: PostMethod, getMethod: GetMethod) {
  val endpoints = postMethod.endpoints :+: getMethod.endpoints
}

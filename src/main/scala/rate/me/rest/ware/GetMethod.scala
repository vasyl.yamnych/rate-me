package rate.me.rest.ware
import cats.data.NonEmptyList
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import io.finch._
import monix.eval.Task
import rate.me.MonixEndpointModule.get
import rate.me.handler.HandlerP2EmptyBody
import rate.me.rest.account.General.AccountId
import rate.me.rest.ware.General._
import rate.me.rest.ware.GetMethod.GetResp
import shapeless._

case class GetMethod(handler: HandlerP2EmptyBody[Task, AccountId, NonEmptyList[WareId], GetResp]) {
  val endpoints: Endpoint[Task, GetResp] = get(waresPathWithParams).mapAsync {
    case accountId :: nel :: HNil =>
      handler(accountId, nel)
  }
}

object GetMethod {
  case class Ware(accountId: AccountId, id: WareId, name: WareName, imageUri: ImageUri)
  case class GetResp(wares: Array[Ware])

  implicit val decodeWare: Decoder[Ware] = deriveDecoder[Ware]
  implicit val encodeWare: Encoder[Ware] = deriveEncoder[Ware]

  implicit val decodeGetResp: Decoder[GetResp] = deriveDecoder[GetResp]
  implicit val encodeGetResp: Encoder[GetResp] = deriveEncoder[GetResp]
}

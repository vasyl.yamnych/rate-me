package rate.me.rest.ware

case class AllMethods(postMethod: PostMethod, getMethod: GetMethod) {
  val endpoints = postMethod.endpoints :+: getMethod.endpoints
}

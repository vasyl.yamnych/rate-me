package rate.me.rest.ware
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import io.finch._
import io.finch.circe._
import monix.eval.Task
import rate.me.MonixEndpointModule.{jsonBody, post}
import rate.me.handler.HandlerP1
import rate.me.rest.account.General.AccountId
import rate.me.rest.ware.General._
import rate.me.rest.ware.PostMethod._
import shapeless._

case class PostMethod(handler: HandlerP1[Task, AccountId, PostReq, PostResp]) {
  val endpoints: Endpoint[Task, PostResp] = post(waresPath :: jsonBody[PostReq]).mapAsync {
    case accountId :: req :: HNil =>
      handler(accountId, req)
  }
}

object PostMethod {
  case class PostReq(name: WareName, image: FullImage)
  case class PostResp(id: WareId)

  implicit val decodePostReq: Decoder[PostReq] = deriveDecoder[PostReq]
  implicit val encodePostReq: Encoder[PostReq] = deriveEncoder[PostReq]

  implicit val decodePostResp: Decoder[PostResp] = deriveDecoder[PostResp]
  implicit val encodePostResp: Encoder[PostResp] = deriveEncoder[PostResp]
}

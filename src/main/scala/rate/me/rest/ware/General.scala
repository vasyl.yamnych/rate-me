package rate.me.rest.ware
import io.circe.{Decoder, Encoder}
import io.finch.DecodePath
import rate.me.MonixEndpointModule._
import rate.me.rest.account.General.AccountId

object General {
  val waresPath = "accounts" :: path[AccountId] :: "wares" :: pathEmpty
  val waresPathWithParams = waresPath :: paramsNel[WareId]("ids")

  implicit def accountIdDecodePath(implicit dec: DecodePath[Int]): DecodePath[AccountId] =
    DecodePath.instance[AccountId] { s: String =>
      dec(s).map(AccountId)
    }

  implicit def wareIdDecodePath(implicit dec: DecodePath[Int]): DecodePath[WareId] =
    DecodePath.instance[WareId] { s: String =>
      dec(s).map(WareId)
    }

  case class WareName(value: String) extends AnyVal
  case class WareId(value: Int) extends AnyVal
  case class FullImage(value: String) extends AnyVal
  case class ImageUri(value: String) extends AnyVal

  implicit val decodeWareName: Decoder[WareName] =
    Decoder.decodeString.emap[WareName](v => Right(WareName(v)))
  implicit val encodeWareName: Encoder[WareName] =
    Encoder.encodeString.contramap[WareName](_.value)

  implicit val decodeWareId: Decoder[WareId] =
    Decoder.decodeInt.emap[WareId](v => Right(WareId(v)))
  implicit val encodeWareId: Encoder[WareId] =
    Encoder.encodeInt.contramap[WareId](_.value)

  implicit val decodeFullImage: Decoder[FullImage] =
    Decoder.decodeString.emap[FullImage](v => Right(FullImage(v)))
  implicit val encodeFullImage: Encoder[FullImage] =
    Encoder.encodeString.contramap[FullImage](_.value)

  implicit val decodeImageUri: Decoder[ImageUri] =
    Decoder.decodeString.emap[ImageUri](v => Right(ImageUri(v)))
  implicit val encodeImageUri: Encoder[ImageUri] =
    Encoder.encodeString.contramap[ImageUri](_.value)
}

package rate.me.simulation

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import rate.me.config.Service
import pureconfig.generic.auto._

object Utils {

  case class ServiceConf(service: Service)

  val serviceConf = pureconfig.loadConfig[ServiceConf].right.get.service

  val httpProtocol = http
    .baseUrl(s"http://${serviceConf.uri}") // root for all relative URLs
    .header(HttpHeaderNames.ContentType, HttpHeaderValues.ApplicationJson)
    .header(HttpHeaderNames.Accept, HttpHeaderValues.ApplicationJson)
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  def post(uri: String, body: String, resultKey: String, counter: String = "") =
    http(s"Create a ware")
      .post(uri)
      .body(StringBody(body))
      .asJson
      .check(status.is(200)) //TODO created 201
      .check(jsonPath("$..id").ofType[Int].saveAs(resultKey))

  def get(uri: String, resultKeyExpr: String) =
    http("Retrieve a ware")
      .get(uri)
      .queryParam("ids", resultKeyExpr)
      .check(status.is(200))
}

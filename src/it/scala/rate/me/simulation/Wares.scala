package rate.me.simulation

import io.circe.generic.auto._
import io.circe.syntax._
import io.gatling.core.Predef._
import rate.me.simulation.Utils._
import rate.me.simulation.Wares._

import scala.concurrent.duration._

class Wares extends Simulation {

  val accPost = exec(post(Accounts.accountsUrl, Accounts.accountBodyReq.asJson.noSpaces, Accounts.accountIdKey))

  val create = accPost.exec(post(waresUrl, wareBodyReq.asJson.noSpaces, wareIdKey))
  val check = accPost.exec(get(waresUrl, wareIdKeyExpr))
  val createAndCheck = accPost.repeat(20, "n") {
    exec(post(waresUrl, wareBodyReq.asJson.noSpaces, wareIdKey, "{n}"))
      .pause(1.second)
      .exec(get(waresUrl, wareIdKeyExpr))
  }

  val itCheck = scenario("Integration check").exec(create, check)
  val makers = scenario("Makers").exec(createAndCheck)

  setUp(itCheck.inject(atOnceUsers(1)), makers.inject(rampUsers(100).during(3.seconds))).protocols(httpProtocol)
}

object Wares {
  case class WareBodyReq(name: String, image: String)
  val wareBodyReq = WareBodyReq("name_gatling", "image_gatling")

  val wareIdKey = "wareId"
  val wareIdKeyExpr = "${" + wareIdKey + "}"
  val waresUrl = "/accounts/" + Accounts.accountIdKeyExpr + "/wares"
}

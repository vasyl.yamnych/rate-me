package rate.me.simulation

import io.circe.generic.auto._
import io.circe.syntax._
import io.gatling.core.Predef._
import rate.me.simulation.Accounts._
import rate.me.simulation.Utils._

import scala.concurrent.duration._

class Accounts extends Simulation {

  val create = exec(post(accountsUrl, accountBodyReq.asJson.noSpaces, accountIdKey))
  val check = exec(get(accountsUrl, accountIdKeyExpr))
  val createAndCheck = repeat(5, "n") {
    exec(post(accountsUrl, accountBodyReq.asJson.noSpaces, accountIdKey, "{n}"))
      .pause(1.second)
      .exec(get(accountsUrl, accountIdKeyExpr))
  }

  val itCheck = scenario("Integration check").exec(create, check)
  val makers = scenario("Makers").exec(createAndCheck)

  setUp(itCheck.inject(atOnceUsers(1)), makers.inject(rampUsers(100).during(3.seconds))).protocols(httpProtocol)
}

object Accounts {
  case class AccountBodyReq(name: String)
  val accountBodyReq = AccountBodyReq("account_name_gatling")

  val accountIdKey = "accountId"
  val accountIdKeyExpr = "${" + accountIdKey + "}"
  val accountsUrl = "/accounts"
}

docker ps                                   - get the name of the existing container
docker ps -f name=<container name>          - get all containers with selected named

docker exec -it <container name> /bin/bash  - get a bash shell in the container
docker exec -it <container name> <command>  - execute whatever command you specify in the container

docker volume create <volume name>          - create a volume
docker volume ls                            - get all volumes
docker volume inspect <volume name>         - show a volume details

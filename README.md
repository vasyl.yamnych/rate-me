# RateMe

RateMe is an app which aims to help producers to make their products better by gathering private customer's feedback


## App API

https://scalafiddle.io/sf/dZgHqUo/12
```
==== ==== ==== === === === 
POST /accounts - create account
POST /accounts {
  "name" : "Gryshevskiy"
}
Response 
{
  "id" : 100
}
==== ==== ==== ===  === === 
POST /accounts/<acc_id>/products - create product
POST /accounts/<acc_id>/products {
  "name" : "medalyons",
  "image" : "Base64Picture"
}
Response 
{
  "id" : 80
}
==== ==== ==== ===  === === 
GET /accounts/<acc_id>/products - get all account's products
GET /accounts/<acc_id>/products
Response 
{
  "products" : [
    {
      "id" : 80,
      "name" : "medalyons",
      "imageUri" : "http://image1.jpg"
    },
    {
      "id" : 90,
      "name" : "pork with mashrooms",
      "imageUri" : "http://pork.jpg"
    }
  ]
}
==== ==== ==== ===  === === 
PUT /accounst/<acc_id>/products/<prod_id> - update product
PUT /accounts/<acc_id>/products/<prod_id> {
  "name" : "medalyons",
  "imageUri" : "http://medalyons.jpg"
}
Response 
{}
==== ==== ==== ===  === === 
POST /accounts/<acc_id>/rates - create categories by which users will rates products
POST /accounts/<acc_id>/rates {
  "name" : "tasty"
}
Response 
{
  "id" : 500
}
==== ==== ==== ===  === === 
GET /accounts/<acc_id>/rates - get all account's rating categories
GET /accounts/<acc_id>/rates
Response 
{
  "rates" : [
    {
      "id" : 500,
      "name" : "tasky"
    },
    {
      "id" : 600,
      "name" : "presentation"
    }
  ]
}
==== ==== ==== ===  === === 
PUT /accounts/<acc_id>/rates/<rate_id> - update rating category
PUT /accounts/<acc_id>/rates/<rate_id> {
  "name" : "service"
}
Response 
{}
==== ==== ==== ===  === === 
DELETE /accounts/<acc_id>/rates/<rate_id> - delete rating category
DELETE /accounts/<acc_id>/rates/<rate_id>
Response 
{}
==== ==== ==== ===  === === 
POST /accounts/<acc_id>/clients	- create client
POST /accounts/<acc_id>/clients {}
Response 
{
  "id" : 12
}
==== ==== ==== ===  === === 
GET /accounts/<acc_id>/clients - get client
GET /accounts/<acc_id>/clients
Response 
{
  "clients" : [
    {
      "id" : 12
    },
    {
      "id" : 13
    }
  ]
}
==== ==== ==== ===  === === 
GET /accounts/<acc_id>/clients/<client_id>/products - get products which user can rates
GET /accounts/<acc_id>/clients/<client_id>/products
Response 
{
  "products" : [
    {
      "id" : 80,
      "name" : "medalyons",
      "imageUri" : "http://image1.jpg"
    },
    {
      "id" : 90,
      "name" : "pork with mashrooms",
      "imageUri" : "http://pork.jpg"
    }
  ]
}
==== ==== ==== ===  === === 
PUT /accounts/<acc_id>/clients/<client_id>/products/<prod_id>/rates - idempotent user's rate of a product
PUT /accounts/<acc_id>/clients/<client_id>/products/<prod_id>/rates {
  "productId" : 80,
  "rateId" : 500,
  "rate" : 4
}
Response 
{}
==== ==== ==== ===  === === 
GET /accounts/<acc_id>/client-rates - get client rates of products
GET /accounts/<acc_id>/client-rates
Response 
{
  "products" : [
    {
      "id" : 80,
      "rates" : [
        {
          "id" : 500,
          "values" : [
            4,
            4,
            5,
            3
          ]
        },
        {
          "id" : 600,
          "values" : [
            2,
            2,
            5
          ]
        }
      ]
    },
    {
      "id" : 90,
      "rates" : [
        {
          "id" : 500,
          "values" : [
            4
          ]
        }
      ]
    }
  ]
}
```
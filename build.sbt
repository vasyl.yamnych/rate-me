import Dependencies._

lazy val commonSettings = Seq(
  scalaVersion := "2.12.8",
  organization := "rate.me",
  scalacOptions ++= Seq(
    "-language:implicitConversions",
    "-language:higherKinds",
    "-language:existentials",
    "-language:postfixOps"
  )
)

lazy val rateMe = (project in file("."))
  .configs(IntegrationTest)
  .enablePlugins(JavaAppPackaging, DockerPlugin, GatlingPlugin)
  .settings(
    commonSettings,
    Defaults.itSettings,
    name := "rate-me",
    version := "0.1",
    mainClass in Compile := Some("rate.me.Main"),
    libraryDependencies ++= Seq(
      pureconfig,
      logback,
      scalaLogging,
      finchXCore,
      finchXCirce,
      circe,
      monix,
      doobieCore,
      doobiePostgres,
      doobieTest,
      scalaTest,
      gatlingCharts,
      gatlingTest
    )
  )
